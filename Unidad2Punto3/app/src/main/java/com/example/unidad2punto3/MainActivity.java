package com.example.unidad2punto3;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button normal = findViewById(R.id.btnNormal);
        Button alReves = findViewById(R.id.btnReves);
        EditText cadena = findViewById(R.id.txtCadena);

        normal.setOnClickListener(view -> {
            imprimir(cadena.getText().toString(), true);
        });
        alReves.setOnClickListener(view -> {
            imprimir(cadena.getText().toString(), false);
        });
    }

    private void imprimir(String cadena, boolean normal) {
        if(!cadena.isEmpty()) {
            if (normal) {
                Toast.makeText(this, cadena, Toast.LENGTH_SHORT).show();
            } else {
                StringBuilder strb = new StringBuilder(cadena);
                cadena = strb.reverse().toString();
                Toast.makeText(this, cadena, Toast.LENGTH_SHORT).show();
            }
        }
    }
}