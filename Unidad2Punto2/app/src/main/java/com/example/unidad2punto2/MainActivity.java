package com.example.unidad2punto2;

import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private EditText dado1;
    private EditText dado2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dado1 = findViewById(R.id.txtDado1);
        dado2 = findViewById(R.id.txtDado2);

        Button jugar = findViewById(R.id.btnJugar);
        jugar.setOnClickListener(view -> {
            generarNumerosAleatorios();
        });
    }

    public void generarNumerosAleatorios() {
        int numero1 = getRandomNumber(1, 6);
        int numero2 = getRandomNumber(1, 6);

        dado1.setText("El Dado nùmero 1 cayó: " + numero1);
        dado2.setText("El Dado nùmero 2 cayó: " + numero2);
    }

    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }
}