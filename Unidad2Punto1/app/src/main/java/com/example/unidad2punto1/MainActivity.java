package com.example.unidad2punto1;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText numero = findViewById(R.id.txtNumero);
        Button calcular = findViewById(R.id.btnCalcular);
        EditText resultado = findViewById(R.id.txtResultado);

        calcular.setOnClickListener(view -> {
            if(esNumero(numero.getText().toString())){
                resultado.setText("El cubo del nùmero " +
                        numero.getText().toString() +
                        " es: " +
                        cuboDelNumero(Integer.parseInt(numero.getText().toString())));
            }
        });
    }

    private boolean esNumero(String numero) {
        try {
            Integer.parseInt(numero);
            return true;
        }catch (NumberFormatException ex){
            return false;
        }
    }

    private Integer cuboDelNumero(Integer numero) {
        Integer resultado = numero * numero * numero;
        if(resultado >= 100 ){
            Toast.makeText(this, "Eres muy afortunado", Toast.LENGTH_SHORT).show();
        }
        return resultado;
    }
}